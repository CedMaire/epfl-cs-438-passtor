module gitlab.gnugen.ch/gmichel/passtor

go 1.13

require (
	github.com/atotto/clipboard v0.1.2
	github.com/rivo/tview v0.0.0-20200108161608-1316ea7a4b35
	github.com/sethvargo/go-diceware v0.2.0
	go.dedis.ch/protobuf v1.0.11
	golang.org/x/crypto v0.0.0-20200117160349-530e935923ad
)
